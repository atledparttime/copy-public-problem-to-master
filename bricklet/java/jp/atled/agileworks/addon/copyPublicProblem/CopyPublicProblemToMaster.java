package jp.atled.agileworks.addon.copyPublicProblem;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import jp.atled.workscape.papi.annotations.PapiInject;
import jp.atled.workscape.papi.annotations.Procedure;
import jp.atled.workscape.papi.model.common.EntryModel;
import jp.atled.workscape.papi.model.common.EnumStatusType;
import jp.atled.workscape.papi.model.common.MessageModel;
import jp.atled.workscape.papi.model.common.RecordListModel;
import jp.atled.workscape.papi.model.common.RecordModel;
import jp.atled.workscape.papi.model.common.ResponseModel;
import jp.atled.workscape.papi.model.common.ResultSetModel;
import jp.atled.workscape.papi.model.common.ResultStatusModel;
import jp.atled.workscape.papi.model.doc.DocDataModel;
import jp.atled.workscape.papi.model.doc.DocModel;
import jp.atled.workscape.papi.model.doc.DocResponseModel;
import jp.atled.workscape.papi.model.doc.DocViewColumnModel;
import jp.atled.workscape.papi.model.doc.EnumDocViewColumnType;
import jp.atled.workscape.papi.model.doc.SelectDocConditionModel;
import jp.atled.workscape.papi.model.doc.SelectDocRequestModel;
import jp.atled.workscape.papi.model.plugin.CursorEventRequestModel;
import jp.atled.workscape.papi.model.plugin.CursorEventResponseModel;
import jp.atled.workscape.papi.service.doc.DocService;
import jp.atled.workscape.papi.service.logging.BrickletLogger;
import jp.atled.workscape.papi.service.system.GatewayService;

public class CopyPublicProblemToMaster {
  private interface Strategy {
    //マスタ書類に転記元書類データをコピーする
    public void copyDataToMaster(final DocModel masterDoc, final DocDataModel originalData);

    //各Procedureの"修正"または"更新"の単語を得る
    public String getWordOfPurpose();

  }

  private class CopyingDocumentStrategy implements Strategy {

    public void copyDataToMaster(final DocModel masterDoc, final DocDataModel originalData) {
      masterDoc.setDocData(originalData);
    }

    public String getWordOfPurpose() {
      return "修正";
    }

  }

  private class UpdatingResolvedVertionStrategy implements Strategy {

    public void copyDataToMaster(final DocModel masterDoc, final DocDataModel originalData) {
      final DocDataModel masterDocData = masterDoc.getDocData();
      printInformationMessage("旧解決バージョン :" + masterDocData.getFieldData("resolved_ver"));
      printInformationMessage("確定解決バージョン：" + originalData.getFieldData("resolved_ver"));
      masterDocData.setFieldData("resolved_ver", originalData.getFieldData("resolved_ver"));
    }

    public String getWordOfPurpose() {
      return "更新";
    }

  }

  @PapiInject
  DocService docSerVice;

  @PapiInject
  GatewayService gatewayService;

  @PapiInject
  BrickletLogger logger;

  @Procedure
  public void copyPublicProblem(CursorEventRequestModel request, CursorEventResponseModel response) {
    printInformationMessage("修正ステップに到達しました。既知の不具合シートの修正を行います。");
    try {
      run(request, new CopyingDocumentStrategy());
    } catch (Exception e) {
      logger.error(e.getMessage());
      setResponseFailStatus(response, e);
    }
    printInformationMessage("AW既知の不具合/制限事項(マスタ)の修正を終了します。");
  }

  @Procedure
  public void copyResolvedVer(CursorEventRequestModel request, CursorEventResponseModel response) {
    printInformationMessage("更新ステップに到達しました。既知の不具合シートの解決バージョンの更新を行います。");
    try {
      run(request, new UpdatingResolvedVertionStrategy());
    } catch (Exception e) {
      logger.error(e.getMessage());
      setResponseFailStatus(response, e);
    }
    printInformationMessage("AW既知の不具合/制限事項(マスタ)の解決バージョンの更新を終了します。");
  }

  //CursorEventResponseModel に失敗の情報を与える
  private void setResponseFailStatus(CursorEventResponseModel response, Exception e) {
    ResultStatusModel result = response.getResultStatus();
    result.setStatus(EnumStatusType.FAIL);
    MessageModel message = result.getMessageList().add();
    message.setCode("faild");
    message.setText(e.getMessage());
    response.setResultStatus(result);
  }

  private void run(final CursorEventRequestModel request, final Strategy strategy) throws Exception {
    final Long docID = request.getWorkflowInfo().getDocId();
    final DocResponseModel docResponse = _c(docSerVice.getDoc(docID));
    final DocModel originalDocument = docResponse.getDoc();
    final DocDataModel originalDocData = originalDocument.getDocData();
    final String adminNo = originalDocument.getAdminNo();

    if (StringUtils.isEmpty(adminNo)) {
      throw new Exception("管理番号が入力されていませんでした。");
    }
    final RecordListModel searchedMasterDocIds = searchMaster(adminNo);
    final List<Long> masterDocIdList = getDocIdList(searchedMasterDocIds);
    if (searchedMasterDocIds.isEmpty()) {
      throw new Exception(strategy.getWordOfPurpose() + "対象の書類が見つかりませんでした。(管理番号:" + adminNo + ")");
    }

    printSearchResults(masterDocIdList, adminNo);
    copyDataToMaster(masterDocIdList, originalDocData, strategy);
    printInformationMessage(strategy.getWordOfPurpose() + "しました。");
  }

  //マスタ書類にコピーする
  private void copyDataToMaster(final List<Long> masterDocIdList, final DocDataModel originalData, final Strategy strategy) {
    final List<DocModel> masterDocumentsList = getMasterDocumentsList(masterDocIdList);
    for (DocModel masterDocument : masterDocumentsList) {
      strategy.copyDataToMaster(masterDocument, originalData);
      _c(docSerVice.updateDoc(masterDocument));
    }
  }

  //検索したマスタ書類のリストデータを取得する
  private List<DocModel> getMasterDocumentsList(final List<Long> masterDocIdList) {
    final List<DocModel> masterDocuments = new ArrayList<DocModel>(masterDocIdList.size());
    for (Long id : masterDocIdList) {
      DocResponseModel docResponse = _c(docSerVice.getDoc(id));
      masterDocuments.add(docResponse.getDoc());
    }

    return masterDocuments;
  }

  //マスターを検索
  private RecordListModel searchMaster(final String adminNo) {
    final SelectDocRequestModel selectDocRequest = docSerVice.createRequestModel(SelectDocRequestModel.class);
    //結果項目を設定
    setDocViewColumn(selectDocRequest);
    //検索条件設定
    setDocSearchCondition(selectDocRequest, adminNo);

    final ResultSetModel resultSet = _c(docSerVice.selectDoc(selectDocRequest)).getResultSet();
    return resultSet.getRecordList();
  }

  //検索条件（フォームコード：public_problem、書類管理番号:adminNo）
  private void setDocSearchCondition(final SelectDocRequestModel selectDocRequest, final String adminNo) {
    SelectDocConditionModel condition = selectDocRequest.getCondition();
    condition.setFormCode("public_problem");
    condition.setAdminNo(adminNo);
  }

  //マスタの検索結果項目を設定（書類ID）
  private void setDocViewColumn(final SelectDocRequestModel selectDocRequest) {
    selectDocRequest.getDocView().setFormCode("public_problem");
    DocViewColumnModel docId = selectDocRequest.getDocView().getColumnList().add();
    docId.setType(EnumDocViewColumnType.DOC_ID);
  }

  //検索結果RecordListをList<Long>化する
  private List<Long> getDocIdList(final RecordListModel searchedMasterDocIds) {
    List<Long> docIdList = new ArrayList<Long>(searchedMasterDocIds.size());
    for (RecordModel result : searchedMasterDocIds) {
      final EntryModel entry = result.getEntryList().get(0);
      docIdList.add(Long.valueOf(entry.getValue().toString()));
    }

    return docIdList;
  }

  //検索結果を出力する
  private void printSearchResults(final List<Long> docIdList, final String adminNo) {
    StringBuilder infoSentence = new StringBuilder();
    for (Long id : docIdList) {
      infoSentence.append(id.toString() + "\n");
    }

    printInformationMessage("対象の書類は" + docIdList.size() + "件あります。以下の通りです。\n書類ID\n" + infoSentence.toString());
  }

  //インフォメーションメッセージを出力
  private void printInformationMessage(final String messageTextString) {
    if (logger.isInfoEnabled()) {
      logger.info(messageTextString);
    }
  }

  /**
   * レスポンスエラーチェック
   * @param response
   * @return
   */
  private static <R extends ResponseModel> R _c(final R response) {
    if (!response.getResultStatus().isSuccess()) {
      throw new PapiRuntimeException(response.getResultStatus());
    }

    return response;
  }
}
