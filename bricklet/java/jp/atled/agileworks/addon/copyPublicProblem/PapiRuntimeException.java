package jp.atled.agileworks.addon.copyPublicProblem;

import jp.atled.workscape.papi.model.common.ResultStatusModel;

public class PapiRuntimeException extends RuntimeException {
	private ResultStatusModel resultStatus;

	public PapiRuntimeException(final ResultStatusModel resultStatus) {
		this.resultStatus = resultStatus;
	}

	public ResultStatusModel getResultStatus() {
		return resultStatus;
	}

}
